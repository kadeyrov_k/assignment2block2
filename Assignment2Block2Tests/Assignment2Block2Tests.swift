//
//  Assignment2Block2Tests.swift
//  Assignment2Block2Tests
//
//  Created by Kadir Kadyrov on 09.07.2020.
//  Copyright © 2020 Kadir Kadyrov. All rights reserved.
//

import XCTest
@testable import Assignment2Block2

class Assignment2Block2Tests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testProblem1() throws {
        let vc: ViewController! = ViewController()
        
        XCTAssert(vc.problem1(startPrice: 24, startYear: 2000, currentYear: 2010, rate: 6).distance(to: 42.98) < 0.1)
        
        XCTAssert(vc.problem1(startPrice: 360, startYear: 2000, currentYear: 2016, rate: 11).distance(to: 1911.92) < 0.1)
        
        XCTAssert(vc.problem1(startPrice: 360, startYear: 2000, currentYear: 2026, rate: 21).distance(to: 51135.46) < 0.1)
    }
    
    func testProblem4() throws {
        let vc: ViewController! = ViewController()
        
        XCTAssert(vc.problem4(number: 52) == 25)
        XCTAssert(vc.problem4(number: 10272) == 27201)
        XCTAssert(vc.problem4(number: 91) == 19)
        XCTAssert(vc.problem4(number: 190) == 91)
        
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
