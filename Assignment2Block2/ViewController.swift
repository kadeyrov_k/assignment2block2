//
//  ViewController.swift
//  Assignment2Block2
//
//  Created by Kadir Kadyrov on 09.07.2020.
//  Copyright © 2020 Kadir Kadyrov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        print(problem1(startPrice: 24, startYear: 1826, currentYear: 2020, rate: 6))
        print(problem2(salery: 700, expenses: 1000, inflation: 3, monthesToLive: 10))
        print(problem3(salery: 700, expenses: 1000, inflation: 3, startMoney: 2400))
        print(problem4(number: 52))
    }
    
    func problem1 (startPrice a: Int, startYear: Int, currentYear: Int, rate p: Int) -> Double{
        let k = Double(a) * pow(1 + 0.01 * Double(p), Double(currentYear - startYear))
        return k
    }
    
    func problem2 (salery x: Int, expenses y: Int, inflation p: Int, monthesToLive m: Int) -> Double {
        var exp = 1000.0
        for currMouth in 1...m {
            exp += problem1(startPrice: y, startYear: 0, currentYear: currMouth, rate: p)
        }

        return exp - Double(x * m)
    }
    
    func problem3 (salery x: Int, expenses y: Int, inflation p: Int, startMoney: Int) -> Int {
        var money = startMoney + x - y, month = 1
        
        while money > 0 {
            money += x
            money -= Int(problem1(startPrice: y, startYear: 0, currentYear: month, rate: p))
            month += 1
        }
        
        return month
    }
    
    func problem4 (number: Int) -> Int {
        var n = number, reverseNumber = 0
        
        while n > 0 {
            reverseNumber = reverseNumber * 10 + n % 10
            n /= 10
        }
        
        return reverseNumber
    }

}

